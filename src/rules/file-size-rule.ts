import * as bytes from 'bytes';
import {IRuleModule} from '../interfaces';
import {PhysicalDirectory} from '../physical/physical-directory';
import {PhysicalFile} from "../physical/physical-file";

export interface IFileSizeRuleOptions {
    min?: string | number;
    max?: string | number;
}

export class FileSizeRule implements IRuleModule {
    public run(folder: PhysicalDirectory, options: IFileSizeRuleOptions): PhysicalFile[] {
        return [];
    }
}
