import {IRuleModule, RuleType} from '../interfaces';
import {FileExtensionRule} from './file-extension-rule';
import {FileSizeRule} from './file-size-rule';

const instances = {};

export function getRuleModule(type: RuleType): IRuleModule | undefined {
    return instances[type] || undefined;
}

function init() {
    instances[RuleType.FileExtension] = new FileExtensionRule();
    instances[RuleType.FileSize] = new FileSizeRule();
}

init();
