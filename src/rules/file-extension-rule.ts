import {IRuleModule} from '../interfaces';
import {PhysicalDirectory} from '../physical/physical-directory';
import {PhysicalFile} from '../physical/physical-file';

export interface IFileExtensionRuleOptions {
    allow?: string[];
    forbid?: string[];
}

export class FileExtensionRule implements IRuleModule {
    public run(folder: PhysicalDirectory, options: IFileExtensionRuleOptions): PhysicalFile[] {
        const target: PhysicalFile[] = [];

        folder.files.forEach(file => this.runFile(file, options) ? target.push(file) : 0);

        return target;
    }

    private runFile(file: PhysicalFile, options: IFileExtensionRuleOptions): boolean {
        const realExtension = file.extension.substring(1);

        if (options.allow && options.allow.indexOf(realExtension) < 0) {
            return true;
        }

        if (options.forbid && options.forbid.indexOf(realExtension) >= 0) {
            return true;
        }

        return false;
    }
}
