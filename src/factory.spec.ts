import {expect} from 'chai';
import 'mocha';
import {Factory} from "./factory";
import {IConfig, RuleType} from "./interfaces";

describe('Factory', () => {
    it('should create a basic config', () => {
        const facto = new Factory();
        const conf = facto.constructConfig(configBasic, 'dir-test');

        expect(conf.path).to.eq('dir-test');
        expect(conf.name).to.eq(undefined);
        expect(conf.id).to.eq(undefined);
        expect(conf.description).to.eq(undefined);
        expect(conf.author).to.eq(undefined);

        expect(conf.rules.length).to.eq(0);
        expect(conf.folders.length).to.eq(0);
        expect(conf.resolvers.length).to.eq(0);
        expect(conf.nodes.length).to.eq(4);
    });

    it('should create a decorated config', () => {
        const facto = new Factory();
        const conf = facto.constructConfig(config1, 'dir-test');

        expect(conf.path).to.eq('dir-test');
        expect(conf.name).to.eq(config1.name);
        expect(conf.id).to.eq(config1.id);
        expect(conf.description).to.eq(config1.description);
        expect(conf.author).to.eq(config1.author);

        expect(conf.rules.length).to.eq(1);
        expect(conf.folders.length).to.eq(0);
        expect(conf.resolvers.length).to.eq(0);
        expect(conf.nodes.length).to.eq(0);
    });

    it('should create a config with multiple rules', () => {
        const facto = new Factory();
        const conf = facto.constructConfig(configMultipleRules, 'dir-test');

        expect(conf.path).to.eq('dir-test');
        expect(conf.name).to.eq(undefined);
        expect(conf.id).to.eq(undefined);
        expect(conf.description).to.eq(undefined);
        expect(conf.author).to.eq(undefined);

        expect(conf.rules.length).to.eq(4);
        expect(conf.folders.length).to.eq(0);
        expect(conf.resolvers.length).to.eq(0);
        expect(conf.nodes.length).to.eq(0);
    });

    it('should create a config with nested rules', () => {
        const facto = new Factory();
        const conf = facto.constructConfig(configNestedRules, 'dir-test');

        expect(conf.path).to.eq('dir-test');
        expect(conf.name).to.eq(undefined);
        expect(conf.id).to.eq(undefined);
        expect(conf.description).to.eq(undefined);
        expect(conf.author).to.eq(undefined);

        expect(conf.rules.length).to.eq(2);
        expect(conf.rules[0].id).to.eq('rule');
        expect(conf.rules[0].rules.length).to.eq(2);
        expect(conf.rules[1].id).to.eq('rule2');
        expect(conf.rules[1].rules.length).to.eq(2);

        expect(conf.folders.length).to.eq(0);
        expect(conf.resolvers.length).to.eq(0);
        expect(conf.nodes.length).to.eq(0);
    });

    it('should create a config with referenced rules', () => {
        const facto = new Factory();
        const conf = facto.constructConfig(configRuleRefs, 'dir-test');

        expect(conf.path).to.eq('dir-test');
        expect(conf.name).to.eq(undefined);
        expect(conf.id).to.eq(undefined);
        expect(conf.description).to.eq(undefined);
        expect(conf.author).to.eq(undefined);

        expect(conf.rules.length).to.eq(2);
        expect(conf.rules[0].id).to.eq('rule');
        expect(conf.rules[0].rules.length).to.eq(2);
        expect(conf.rules[1].id).to.eq('rule2');
        expect(conf.rules[1].rules.length).to.eq(2);
        expect(conf.rules[1].rules[0].rules.length).to.eq(2);

        expect(conf.folders.length).to.eq(0);
        expect(conf.resolvers.length).to.eq(0);

        expect(conf.nodes.length).to.eq(1);
        expect(conf.nodes[0].rules.length).to.eq(1);
        expect(conf.nodes[0].rules[0].rules.length).to.eq(2);
        expect(conf.nodes[0].rules[0].rules[0].rules.length).to.eq(2);
    });
});

const configBasic: IConfig = {
    nodes: [
        { name: 'Dir1' },
        { name: 'Dir2' },
        { name: 'Dir3' },
        { name: 'Dir4' },
    ],
};

const config1: IConfig = {
    name: 'Config Decorated',
    description: 'A Decorated Config',
    author: '@Nelgamix',
    id: '@ID',
    rules: [
        { type: RuleType.FileExtension, id: 'rule', options: { allow: ['mp4'] } },
    ],
    folders: [],
    resolvers: [],
    nodes: [],
};

const configMultipleRules: IConfig = {
    rules: [
        { type: RuleType.FileExtension, id: 'rule', options: { allow: ['mp1'] } },
        { type: RuleType.FileSize, id: 'rule2', options: { allow: ['mp2'] } },
        { type: RuleType.FileExtension, id: 'rule3', options: { allow: ['mp3'] } },
        { type: RuleType.FileExtension, id: 'rule4', options: { allow: ['mp4'] } },
    ],
    folders: [],
    resolvers: [],
    nodes: [],
};

const configNestedRules: IConfig = {
    rules: [
        {
            type: RuleType.FileExtension, id: 'rule', options: { allow: ['mp1'] },
            rules: [
                { type: RuleType.FileExtension },
                { type: RuleType.FileExtension },
            ],
        },
        {
            type: RuleType.FileSize, id: 'rule2', options: { allow: ['mp2'] },
            rules: [
                { type: RuleType.FileExtension },
                { type: RuleType.FileExtension },
            ],
        },
    ],
    folders: [],
    resolvers: [],
    nodes: [],
};

const configRuleRefs: IConfig = {
    rules: [
        {
            type: RuleType.FileExtension, id: 'rule', options: { allow: ['mp1'] },
            rules: [
                { type: RuleType.FileExtension },
                { type: RuleType.FileExtension },
            ],
        },
        {
            type: RuleType.FileSize, id: 'rule2', options: { allow: ['mp2'] },
            rules: [
                'rule',
                { type: RuleType.FileExtension },
            ],
        },
    ],
    folders: [],
    resolvers: [],
    nodes: [
        {
            name: 'dir',
            rules: ['rule2'],
        },
    ],
};
