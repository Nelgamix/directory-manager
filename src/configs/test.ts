import {IConfig, RuleType} from '../interfaces';

export const config: IConfig = {
    name: 'TS Config example',
    rules: [
        {
            id: 'extension_video',
            // description: 'Accepts only videos',
            type: RuleType.FileExtension,
            options: {
                allow: ['mp4', 'mkv', 'ts', 'gif', 'gifv', 'webm', 'wmv'],
            },
        },
        {
            id: 'extension_image',
            type: RuleType.FileExtension,
            options: {
                allow: ['png', 'jpeg', 'jpg', 'webp'],
            },
        },
        {
            id: 'extension_music',
            type: RuleType.FileExtension,
            options: {
                allow: ['mp3', 'ogg', 'flac', 'aac', 'wma'],
            },
        },
    ],
    nodes: [
        {
            name: 'Downloads',
            children: [
                { name: 'Browser' },
                { name: 'Torrent' },
            ],
        },
        {
            name: 'VM',
            children: [
                { name: 'Linux' },
                { name: 'MacOS' },
                { name: 'Windows' },
            ],
        },
        {
            name: 'TMP',
            ignore: true,
        },
        {
            name: 'Programs',
            children: [
                {
                    name: 'Game',
                    children: [
                        { name: 'Discord' },
                        { name: 'Steam' },
                        { name: 'Twitch' },
                    ],
                },
                { name: 'SDK' },
                { name: 'Software' },
            ],
        },
        {
            name: 'Documents',
            children: [
                { name: 'Cloud' },
                {
                    name: 'Images',
                    rules: [
                        {
                            refId: 'extension_image',
                            deep: true,
                        },
                    ],
                    children: [
                        { name: 'Artworks' },
                        { name: 'Icons' },
                        { name: 'Memes' },
                        { name: 'Screenshots' },
                        { name: 'Wallpapers' },
                        { name: 'Other' },
                    ],
                },
                {
                    name: 'Musics',
                    rules: [
                        {
                            refId: 'extension_audio',
                            deep: true,
                        },
                    ],
                    children: [],
                },
                {
                    name: 'Videos',
                    rules: [
                        {
                            refId: 'extension_video',
                            deep: true,
                        },
                    ],
                    children: [
                        { name: 'Clips' },
                        { name: 'Memes' },
                        { name: 'Movies' },
                        { name: 'Other' },
                    ],
                },
                {
                    name: 'Office',
                    children: [
                        { name: 'Notes' },
                        { name: 'Excel', rules: [ { type: RuleType.FileExtension, options: { allow: ['xls', 'xlsx'] } } ] },
                        { name: 'Word', rules: [ { type: RuleType.FileExtension, options: { allow: ['doc', 'docx'] } } ] },
                        { name: 'PowerPoint' },
                    ],
                },
                {
                    name: 'Development',
                    children: [
                        {
                            name: 'Desktop',
                            children: [
                                { name: 'C' },
                                { name: 'CPP' },
                                { name: 'Java' },
                            ],
                        },
                        {
                            name: 'Mobile',
                            children: [
                                { name: 'Android' },
                                { name: 'iOS' },
                            ],
                        },
                        {
                            name: 'Web',
                            children: [
                                { name: 'JS' },
                                { name: 'PHP' },
                            ],
                        },
                        {
                            name: 'Script',
                            children: [
                                { name: 'Batch' },
                                { name: 'Python' },
                            ],
                        },
                    ],
                },
            ],
        },
    ],
};
