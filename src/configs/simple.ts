import {IConfig, RuleType} from '../interfaces';

// Config: rules, resolvers, folders and file system description
// Rule: function that analyse a folder to validate its content
// Resolver: function that resolves a conflict raised by a rule
// Folder: a node in the file system that contains rules and resolvers

export const config: IConfig = {
    rules: [
        {
            id: 'ru_videos',
            type: RuleType.FileExtension,
            options: { allow: ['mp4', 'mkv'] },
        },
        {
            id: 'ru_images',
            type: RuleType.FileExtension,
            options: { allow: ['png', 'jpg'] },
        },
        {
            id: 'ru_musics',
            type: RuleType.FileExtension,
            options: { allow: ['mp3', 'aac'] },
        },
        {
            name: 'Move all video files above 25mb',
            id: 'ru_test',
            type: RuleType.Intersect,
            rules: [
                'ru_videos',
                {
                    type: RuleType.FileSize,
                    options: { max: '25mb' },
                },
            ],
        },
    ],
    resolvers: [
        { id: 'rv_videos', moveTo: 'Videos' },
        { id: 'rv_images', moveTo: 'images' },
        { id: 'rv_musics', moveTo: 'musics' },
    ],
    nodes: [
        {
            name: 'Images',
            id: 'images',
            rules: [
                {
                    refId: 'ru_images',
                    deep: true,
                },
            ],
        },
        {
            name: 'Videos',
            id: 'videos',
            rules: ['ru_videos'],
        },
        {
            name: 'Musics',
            id: 'musics',
            rules: ['ru_musics'],
        },
        {
            name: 'Other',
            rules: [
                {
                    refId: 'ru_test',
                    inverse: true,
                },
                {
                    refId: 'ru_videos',
                    inverse: true,
                    resolvers: ['rv_videos'],
                },
                {
                    refId: 'ru_images',
                    inverse: true,
                    resolvers: ['rv_images'],
                },
                {
                    refId: 'ru_musics',
                    inverse: true,
                    resolvers: ['rv_musics'],
                },
            ],
        },
    ],
};
