import * as Debug from 'debug';
import {config} from './configs/simple';
import {Factory} from "./factory";
import {IConfig} from './interfaces';

const debug = Debug('index');

launch(config, './dir-simple', true, true, true);

function launch(iConfig: IConfig, pathToDir: string, debugConfig: boolean, run: boolean, debugRun = false) {
    const factory = new Factory();
    const rConfig = factory.constructConfig(iConfig, pathToDir);

    if (debugConfig) {
        debug(JSON.stringify(rConfig.debug(), null, 4));
    }

    if (run) {
        const items = rConfig.run();
        if (debugRun) {
            debug(JSON.stringify(items, null, 4));
        }
    }
}
