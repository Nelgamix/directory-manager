import {expect} from 'chai';
import * as fs from "fs";
import 'mocha';
import * as rimraf from "rimraf";
import {PhysicalDirectory} from "./physical-directory";
import {PhysicalFile} from "./physical-file";
import {PhysicalFolder} from "./physical-folder";

describe('Physical Directory', () => {
    it('should create a physical directory without a real path', () => {
        const dir = new PhysicalDirectory();
        expect(dir).to.not.be.null;
        expect(dir.files.length).to.eq(0);
        expect(dir.folders.length).to.eq(0);
        expect(dir.exists).to.eq(false);

        dir.files.push(new PhysicalFile('file1'));
        dir.files.push(new PhysicalFile('file2'));
        dir.files.push(new PhysicalFile('file3'));
        dir.folders.push(new PhysicalFolder('folder1'));
        expect(dir.files.length).to.eq(3);
        expect(dir.folders.length).to.eq(1);
    });

    it('should read a real dir correctly', () => {
        const path = 'dir-test/physical-directory';
        fs.mkdirSync(path + '/dir1', {recursive: true});
        fs.mkdirSync(path + '/dir2', {recursive: true});
        fs.mkdirSync(path + '/dir3', {recursive: true});
        fs.writeFileSync(path + '/file1', '');
        fs.writeFileSync(path + '/file2', '');

        const dir = new PhysicalDirectory(path, true);
        expect(dir).to.not.be.null;
        expect(dir.files.length).to.eq(2);
        expect(dir.folders.length).to.eq(3);
        expect(dir.exists).to.eq(true);

        rimraf.sync(path);
    });
});
