import {expect} from 'chai';
import 'mocha';
import {PhysicalFile} from "./physical-file";

describe('Physical File', () => {
    it('should create a physical file without a real path', () => {
        const file = new PhysicalFile('text.txt');
        expect(file).to.not.be.null;
        expect(file.path).to.eq('text.txt');
        expect(file.extension).to.eq('.txt');
        expect(file.baseName).to.eq('text');
    });

    it('should handle files without extensions', () => {
        const file = new PhysicalFile('text');
        expect(file).to.not.be.null;
        expect(file.path).to.eq('text');
        expect(file.extension).to.eq('');
        expect(file.baseName).to.eq('text');
    });
});
