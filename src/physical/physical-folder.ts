import {Stats} from 'fs';
import * as fspath from 'path';

export class PhysicalFolder {
    public baseName: string;
    public size?: number;

    constructor(public path: string, public stats?: Stats) {
        this.baseName = fspath.basename(path);
        this.size = stats ? stats.size : undefined;
    }

    public debug(): any {
        return {
            path: this.path,
        };
    }
}
