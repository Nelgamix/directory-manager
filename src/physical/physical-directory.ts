import * as fs from "fs";
import * as fspath from "path";
import {PhysicalFile} from './physical-file';
import {PhysicalFolder} from './physical-folder';

interface IFileAnalyzed {
    dir: string;
    path: string;
    stats: fs.Stats;
}

function getFiles(filesAnalysed: IFileAnalyzed[]): IFileAnalyzed[] {
    return filesAnalysed.filter((file: IFileAnalyzed) => file.stats.isFile());
}

function getFolders(filesAnalysed: IFileAnalyzed[]): IFileAnalyzed[] {
    return filesAnalysed.filter((file: IFileAnalyzed) => file.stats.isDirectory());
}

export class PhysicalDirectory {
    public exists: boolean;
    public path?: string;
    public files: PhysicalFile[];
    public folders: PhysicalFolder[];

    constructor(path?: string, link?: boolean) {
        this.path = path;
        this.files = [];
        this.folders = [];
        this.exists = false;

        if (link) {
            this.linkToFs();
        }
    }

    public linkToFs(path?: string): boolean {
        const fPath = path || this.path;

        if (!fPath) {
            throw new Error('Can\' link folder to fs since no path was given');
        }

        this.path = fPath;

        if (fs.existsSync(fPath)) {
            this.path = path;
            this.exists = true;

            const filesAnalyzed: IFileAnalyzed[] = fs.readdirSync(fPath).map((file: string) => {
                const filePath = fspath.join(fPath, file);
                return {
                    dir: file,
                    path: filePath,
                    stats: fs.statSync(filePath),
                };
            });

            this.folders = getFolders(filesAnalyzed)
                .map((folder) => new PhysicalFolder(folder.path, folder.stats));

            this.files = getFiles(filesAnalyzed)
                .map((file) => new PhysicalFile(file.path, file.stats));
        } else {
            this.exists = false;
            this.folders = [];
            this.files = [];
        }

        return this.exists;
    }

    public debug(): any {
        return {
            files: this.files.map(file => file.debug()),
            folders: this.folders.map(folder => folder.debug()),
        };
    }
}
