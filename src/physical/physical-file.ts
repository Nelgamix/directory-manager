import {Stats} from 'fs';
import * as fspath from 'path';

export class PhysicalFile {
    public extension: string;
    public baseName: string;
    public size?: number;

    constructor(public path: string, public stats?: Stats) {
        this.extension = fspath.extname(path);
        this.baseName = fspath.basename(path, this.extension);
        this.size = stats ? stats.size : undefined;
    }

    public debug(): any {
        return {
            path: this.path,
        };
    }
}
