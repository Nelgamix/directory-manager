import {expect} from 'chai';
import 'mocha';
import {PhysicalFolder} from "./physical-folder";

describe('Physical Folder', () => {
    it('should create a physical folder without a real path', () => {
        const folder = new PhysicalFolder('folder/subfolder');
        expect(folder).to.not.be.null;
        expect(folder.path).to.eq('folder/subfolder');
        expect(folder.baseName).to.eq('subfolder');
    });
});
