import {PhysicalDirectory} from './physical/physical-directory';
import {PhysicalFile} from './physical/physical-file';
import {IFileExtensionRuleOptions} from './rules/file-extension-rule';
import {IFileSizeRuleOptions} from './rules/file-size-rule';

export type IRuleOptions = IFileSizeRuleOptions | IFileExtensionRuleOptions;

export enum RuleType {
    FileSize = 'file-size',
    FileExtension = 'file-extension',
    Count = 'count',
    Name = 'name',
    Presence = 'presence',
    Intersect = 'intersect',
    Union = 'union',
    Custom = 'custom',
}

export interface IRuleConflict {
    file: PhysicalFile;
    module?: RuleType;
    ruleId?: string;
}

export interface IRuleModule {
    run(folder: PhysicalDirectory, options: any): PhysicalFile[];
}

export interface IElement {
    name?: string;
    id?: string;
    author?: string;
    description?: string;
}

export interface IConfig extends IElement {
    path?: string;
    rules?: TRule[];
    resolvers?: TResolver[];
    folders?: TFolder[];
    nodes: TFolder[];
}

export interface IFolder extends IElement {
    path?: string;
    ignore?: boolean;
    resolvers?: TResolver[];
    rules?: TRule[];
    children?: IFolder[];
}

export interface IRule extends IElement {
    rules?: TRule[];
    resolvers?: TResolver[];
    deep?: boolean;
    ignore?: boolean;
    inverse?: boolean;
    options?: IRuleOptions;
}

export interface IRuleDef extends IRule {
    type: RuleType;
}

export interface IRuleRef extends IRule {
    refId: string;
}

export interface IResolver extends IElement {
    custom?: TCustomFun;
    moveTo?: string;
    delete?: boolean;
    moveItems?: TMoveFun;
    deleteItems?: TDeleteFun;
}

export type TFolder = IFolder | string;

export type TResolver = IResolver | string;

// Must return true if conflict is resolved, false otherwise
export type TCustomFun = ((conflict: IRuleConflict) => boolean);

// Return path where to move the file conflicting
export type TMoveFun = ((conflict: IRuleConflict) => string | undefined);

// Return whether or not to delete the file conflicting
export type TDeleteFun = ((conflict: IRuleConflict) => boolean);

export type TRuleDef = IRuleDef;

export type TRuleRef = IRuleRef | string;

export type TRule = TRuleDef | TRuleRef;
