import * as Debug from 'debug';
import * as fs from "fs";
import {IRuleConflict} from "../interfaces";
import {PhysicalDirectory} from "../physical/physical-directory";
import {PhysicalFolder} from "../physical/physical-folder";
import {Config} from "./config";
import {Element} from './element';
import {Resolver} from "./resolver";
import {Rule} from './rule';

const debug = Debug('index');

export class Folder extends Element {
    public path: string;
    public ignore?: boolean;
    public rules: Rule[];
    public children: Folder[];
    public resolvers: Resolver[];

    public physicalDirectory?: PhysicalDirectory;
    public parent?: Folder;
    public config: Config;

    constructor(path: string, parent: Folder | undefined, config: Config) {
        super();

        this.path = path;
        this.parent = parent;
        this.config = config;
        this.rules = [];
        this.children = [];
        this.resolvers = [];
    }

    public debug(): any {
        return {
            path: this.path,
            ignore: this.ignore,
            rules: this.rules,
            children: this.children.map(child => child.debug()),
            resolvers: this.resolvers,
        };
    }

    public run(): IRuleConflict[] {
        if (!this.physicalDirectory) {
            if (!fs.existsSync(this.path)) {
                fs.mkdirSync(this.path, {recursive: true});
                debug('Created folder ' + this.path);
            }

            this.physicalDirectory = new PhysicalDirectory(this.path, true);
        }

        if (this.ignore) {
            return [];
        }

        const folderPaths = this.getUndescribedFolders();
        const folders = this.constructFolders(folderPaths);
        const deepRules = this.getDeepRules();
        folders.forEach(folder => folder.rules = deepRules);

        const itemsUF = folders.map(folder => folder.run()).flat();
        const itemsR = this.rules.map(rule => rule.run(this)).flat();
        const itemsC = this.children.map(child => child.run()).flat();

        let itemsT = itemsUF.concat(itemsR, itemsC);

        if (this.resolvers) {
            for (const resolver of this.resolvers) {
                itemsT = resolver.run(itemsT, this);
            }
        }

        return itemsT;
    }

    public searchRule(ruleId: string): Rule | undefined {
        const ruleFound = this.rules.find(rule => rule.id === ruleId);
        return ruleFound
            || (this.parent ?
                this.parent.searchRule(ruleId)
                : (this.config ?
                    this.config.searchRule(ruleId)
                    : undefined));
    }

    public searchResolver(id: string): Resolver | undefined {
        const resolverFound = this.resolvers.find(resolver => resolver.id === id);
        return resolverFound
            || (this.parent ?
                this.parent.searchResolver(id)
                : (this.config ?
                    this.config.searchResolver(id)
                    : undefined));
    }

    public searchFolder(id: string): Folder | undefined {
        if (this.id === id) {
            return this;
        }

        for (const f of this.children) {
            const found = f.searchFolder(id);
            if (found) {
                return found;
            }
        }
    }

    public clone(): Folder {
        const folder = new Folder(this.path, this.parent, this.config);

        folder.name = this.name || folder.name;
        folder.description = this.description || folder.description;
        folder.author = this.author || folder.author;

        folder.ignore = this.ignore || folder.ignore;
        folder.rules = this.rules.map(r => r.clone());
        folder.children = this.children.map(r => r.clone());
        folder.resolvers = this.resolvers.map(r => r.clone());

        return folder;
    }

    private constructFolders(paths: string[]): Folder[] {
        return paths.map(path => new Folder(path, this, this.config));
    }

    private getUndescribedFolders(): string[] {
        if (!this.physicalDirectory) {
            return [];
        }

        const folders: PhysicalFolder[] = this.physicalDirectory.folders;
        return this.children
            .filter(child => folders.find(folder => folder.path === child.path))
            .map(found => found.path);
    }

    private getDeepRules(): Rule[] {
        return this.rules.filter(rule => rule.deep);
    }
}
