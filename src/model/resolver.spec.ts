import {expect} from 'chai';
import 'mocha';
import {Resolver} from "./resolver";

describe('Resolver', () => {
    it('should create a basic resolver', () => {
        const resolver = new Resolver();
        expect(resolver).to.not.be.null;
    });
});
