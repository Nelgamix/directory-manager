import {IRuleConflict, IRuleOptions, RuleType} from '../interfaces';
import {PhysicalFile} from "../physical/physical-file";
import {getRuleModule} from '../rules/modules';
import {Element} from './element';
import {Folder} from "./folder";
import {Resolver} from "./resolver";

export function intersect(results: IRuleConflict[][]): IRuleConflict[] {
    // [a, b, c], [b, c], [b, d]
    // a => 2nd doesn't have it
    // b => OK
    // c => 3rd doesn't have it
    // b => already OK
    // c => already NOK
    // b => already OK
    // d => 1st doesn't have it
    // RES: b
    let i, j, src, dst, found;
    const corresp = {};
    const fArr: IRuleConflict[] = [];
    for (i = 0; i < results.length; i++) {
        src = results[i];
        for (const itemSrc of src) {
            if (corresp[itemSrc.file.path] !== false && corresp[itemSrc.file.path] !== true) {
                for (j = 0; j < results.length; j++) {
                    dst = results[j];
                    if (j !== i) {
                        found = false;
                        for (const itemDst of dst) {
                            if (itemDst.file.path === itemSrc.file.path) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            corresp[itemSrc.file.path] = false;
                            break;
                        }
                    }
                }
                if (corresp[itemSrc.file.path] !== false) {
                    corresp[itemSrc.file.path] = true;
                    fArr.push(itemSrc);
                }
            }
        }
    }

    return fArr;
}

export function union(results: IRuleConflict[][]): IRuleConflict[] {
    // [a, b, c], [b, c], [b, d]
    // a => OK
    // b => OK
    // c => OK
    // b => already OK
    // c => already OK
    // b => already OK
    // d => OK
    // RES: a, b, c, d
    const corres = {};
    const fArr: IRuleConflict[] = [];
    for (const arr of results) {
        for (const item of arr) {
            if (!corres[item.file.path]) {
                corres[item.file.path] = true;
                fArr.push(item);
            }
        }
    }
    return fArr;
}

export class Rule extends Element {
    public type: RuleType;
    public deep?: boolean;
    public ignore?: boolean;
    public inverse?: boolean;
    public rules: Rule[];
    public resolvers: Resolver[];
    public options: IRuleOptions;

    constructor(options?: object) {
        super();

        this.options = options || {};
        this.type = RuleType.Custom;
        this.rules = [];
        this.resolvers = [];
    }

    public run(folder: Folder): IRuleConflict[] {
        const results: IRuleConflict[][] = this.rules.map(rule => rule.run(folder));
        let items: IRuleConflict[];

        switch (this.type) {
            case RuleType.Intersect:
                items = intersect(results);
                break;
            case RuleType.Union:
                items = union(results);
                break;
            default:
                const module = getRuleModule(this.type);

                if (module) {
                    let files = module.run(folder.physicalDirectory!, this.options);
                    if (files.length > 0 && this.inverse) {
                        files = this.invertItems(folder.physicalDirectory!.files, files);
                    }
                    items = results.flat().concat(files.map(file => {
                        return {
                            file,
                            module: this.type,
                            ruleId: this.id,
                        };
                    }));
                } else {
                    items = union(results);
                }
        }

        if (items.length > 0) {
            for (const resolver of this.resolvers) {
                items = resolver.run(items, folder);
            }
        }

        return items;
    }

    public clone(): Rule {
        const nr = new Rule();

        // Copy element stuff
        nr.name = this.name ? this.name : nr.name;
        nr.description = this.description ? this.description : nr.description;
        nr.author = this.author ? this.author : nr.author;

        // Copy specific rule attributes
        nr.type = this.type;
        nr.deep = this.deep ? this.deep : nr.deep;
        nr.inverse = this.inverse ? this.inverse : nr.inverse;

        // Copy object attributes
        nr.rules = this.rules.map(r => r.clone());
        nr.resolvers = this.resolvers.map(r => r.clone());
        nr.options = JSON.parse(JSON.stringify(this.options));

        return nr;
    }

    private invertItems(files: PhysicalFile[], conflicted: PhysicalFile[]): PhysicalFile[] {
        return files.filter(file => !conflicted.includes(file));
    }
}
