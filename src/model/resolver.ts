import * as Debug from 'debug';
import * as fs from "fs";
import * as fspath from 'path';
import {IRuleConflict, TCustomFun, TDeleteFun, TMoveFun} from '../interfaces';
import {Element} from './element';
import {Folder} from "./folder";

const debug = Debug('index');

export class Resolver extends Element {
    public moveTo?: string;
    public delete?: boolean;
    public moveItems?: TMoveFun;
    public deleteItems?: TDeleteFun;
    public custom?: TCustomFun;

    public run(conflicts: IRuleConflict[], folder: Folder): IRuleConflict[] {
        const conflictsLeft: IRuleConflict[] = [];

        if (this.moveTo) {
            const path = this.resolvePath(this.moveTo, folder);

            if (path) {
                conflicts.forEach(conflict => {
                    if (!this.conflictMoveTo(conflict, path)) {
                        conflictsLeft.push(conflict);
                    }
                });
            }
        } else if (this.moveItems) {
            conflicts.forEach(conflict => {
                const p = this.moveItems!(conflict);
                if (!p || !this.conflictMoveTo(conflict, p)) {
                    conflictsLeft.push(conflict);
                }
            });
        } else if (this.deleteItems) {
            conflicts.forEach(conflict => {
                const p = this.deleteItems!(conflict);
                if (!p || !this.conflictDelete(conflict)) {
                    conflictsLeft.push(conflict);
                }
            });
        } else if (this.delete) {
            conflicts.forEach(conflict => {
                if (!this.conflictDelete(conflict)) {
                    conflictsLeft.push(conflict);
                }
            });
        } else if (this.custom) {
            conflicts.forEach(conflict => {
                const p = this.custom!(conflict);
                if (!p) {
                    conflictsLeft.push(conflict);
                }
            });
        }

        return conflictsLeft;
    }

    public clone(): Resolver {
        const nr = new Resolver();

        // Copy element stuff
        nr.name = this.name || nr.name;
        nr.description = this.description || nr.description;
        nr.author = this.author || nr.author;

        // Copy Resolver attribs
        nr.moveTo = this.moveTo || nr.moveTo;
        nr.delete = this.delete || nr.delete;
        nr.moveItems = this.moveItems || nr.moveItems;
        nr.deleteItems = this.deleteItems || nr.deleteItems;
        nr.custom = this.custom || nr.custom;

        return nr;
    }

    private conflictMoveTo(conflict: IRuleConflict, path: string): boolean {
        const op = conflict.file.path;
        const np = path + fspath.sep + conflict.file.baseName + conflict.file.extension;

        if (fs.existsSync(op) && fs.existsSync(np)) {
            debug('Moving ' + op + ' to ' + np);
            // fs.renameSync(file.file.path, f.path + '/' + file.file.baseName + file.file.extension);
            return true;
        } else {
            return false;
        }
    }

    private conflictDelete(conflict: IRuleConflict): boolean {
        if (fs.existsSync(conflict.file.path)) {
            debug('Deleting ' + conflict.file.path);
            // fs.unlinkSync(conflict.file.path);
            return true;
        } else {
            return false;
        }
    }

    private resolvePath(idOrPath: string, folder: Folder): string | undefined {
        let path;
        const f = folder.config.searchNode(idOrPath); // Search in folder id's

        // If we did not find it, we need to ask the config for the path
        if (!f) {
            path = folder.config.searchFolderPath(idOrPath);
        } else {
            path = f.path;
        }

        return path;
    }
}
