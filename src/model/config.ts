import * as fs from 'fs';
import * as fspath from 'path';
import {IRuleConflict} from "../interfaces";
import {Element} from './element';
import {Folder} from './folder';
import {Resolver} from "./resolver";
import {Rule} from './rule';

export class Config extends Element {
    public path: string;
    public rules: Rule[];
    public folders: Folder[];
    public resolvers: Resolver[];
    public nodes: Folder[];

    constructor(path: string) {
        super();

        // this.path = fspath.resolve(path);
        this.path = path.replace(/[\/\\]/, fspath.sep);

        this.rules = [];
        this.folders = [];
        this.resolvers = [];
        this.nodes = [];
    }

    public run(): IRuleConflict[] {
        return this.nodes.map(folder => folder.run()).flat();
    }

    public debug(): any {
        return {
            path: this.path,
            rules: this.rules,
            folders: this.folders.map(folder => folder.debug()),
            nodes: this.nodes.map(node => node.debug()),
            resolvers: this.resolvers,
        };
    }

    public searchResolver(id: string): Resolver | undefined {
        return this.resolvers.find(resolver => resolver.id === id);
    }

    public searchRule(refId: string): Rule | undefined {
        return this.rules.find(rule => rule.id === refId);
    }

    public searchFolder(folderId: string): Folder | undefined {
        return this.folders.find(folder => folder.id === folderId);
    }

    public searchNode(id: string): Folder | undefined {
        let node = this.nodes.find(n => n.id === id);
        if (!node) {
            for (const n of this.nodes) {
                node = n.searchFolder(id);
                if (node) {
                    return node;
                }
            }
        }
        return node;
    }

    public searchFolderPath(folderPath: string): string | undefined {
        const rPath = this.path + fspath.sep + folderPath.replace('.', fspath.sep);
        if (fs.existsSync(rPath)) {
            return rPath;
        }
    }
}
