export class Element {
    public name?: string;
    public id?: string;
    public description?: string;
    public author?: string;
}
