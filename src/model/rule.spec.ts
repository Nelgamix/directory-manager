import {expect} from 'chai';
import 'mocha';
import {IRuleConflict} from "../interfaces";
import {PhysicalFile} from '../physical/physical-file';
import {intersect, union} from './rule';

describe('rules', () => {
    const testArr: IRuleConflict[][] = [
        [
            {file: new PhysicalFile('A')},
            {file: new PhysicalFile('B')},
            {file: new PhysicalFile('C')},
        ],
        [
            {file: new PhysicalFile('B')},
            {file: new PhysicalFile('C')},
        ],
        [
            {file: new PhysicalFile('B')},
            {file: new PhysicalFile('D')},
        ],
    ];

    it('should give a correct union', () => {
        const result = union(testArr);
        expect(result.length).to.eq(4);
    });

    it('should give a correct intersect', () => {
        const result = intersect(testArr);
        expect(result.length).to.eq(1);
    });
});
