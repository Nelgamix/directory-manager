import * as fspath from 'path';
import {IConfig, IFolder, IResolver, IRuleDef, IRuleRef, TFolder, TResolver, TRule} from './interfaces';
import {Config} from './model/config';
import {Folder} from './model/folder';
import {Resolver} from "./model/resolver";
import {Rule} from './model/rule';

export class Factory {
    private config!: Config;

    public constructConfig(iConfig: IConfig, path?: string): Config {
        const rPath = path || iConfig.path;
        if (!rPath) {
            throw new Error('The config must have a path');
        }

        const config = new Config(rPath);
        config.author = iConfig.author;
        config.description = iConfig.description;
        config.name = iConfig.name;
        config.id = iConfig.id;
        this.config = config;

        if (iConfig.rules) {
            for (const rule of iConfig.rules) {
                config.rules.push(this.constructRule(rule));
            }
        }

        if (iConfig.folders) {
            for (const folder of iConfig.folders) {
                config.folders.push(this.constructFolder(folder));
            }
        }

        if (iConfig.resolvers) {
            for (const resolver of iConfig.resolvers) {
                config.resolvers.push(this.constructResolver(resolver));
            }
        }

        if (iConfig.nodes) {
            for (const node of iConfig.nodes) {
                config.nodes.push(this.constructFolder(node));
            }
        }

        return config;
    }

    public constructFolder(iFolder: TFolder, parent?: Folder): Folder {
        let folder: Folder;

        if (typeof iFolder === 'string') {
            folder = this.findFolder(iFolder);
        } else {
            folder = this.createFolder(iFolder, parent);
        }

        return folder;
    }

    public constructRule(iRule: TRule, parent?: Folder): Rule {
        let rule: Rule;

        if (typeof iRule === 'string' || (iRule as IRuleRef).refId) {
            // It's a ref
            rule = this.findRule(iRule, parent);
        } else {
            // It's a def
            rule = this.createRule(iRule as IRuleDef, parent);
        }

        return rule;
    }

    public constructResolver(iResolver: TResolver, parent?: Folder): Resolver {
        let resolver: Resolver;
        if (typeof iResolver === 'string') {
            resolver = this.findResolver(iResolver, parent);
        } else {
            resolver = this.createResolver(iResolver);
        }

        return resolver;
    }

    private findFolder(id: string, parent?: Folder): Folder {
        let folder = this.config.searchFolder(id);

        if (folder) {
            folder = folder.clone();
            folder.parent = parent;
            folder.path = (parent ? parent.path : this.config.path) + fspath.sep + folder.path;

            return folder;
        }

        throw new Error('Folder ' + id + ' not found');
    }

    private createFolder(iFolder: IFolder, parent?: Folder): Folder {
        let folder: Folder;

        let rPath = iFolder.path || iFolder.name || iFolder.id;
        if (!rPath) {
            throw new Error('A folder must have either a path, a name or an id set to determine its path');
        }

        rPath = (parent ? parent.path : this.config.path) + fspath.sep + rPath;

        folder = new Folder(rPath, parent, this.config);
        folder.name = iFolder.name;
        folder.id = iFolder.id;
        folder.description = iFolder.description;
        folder.author = iFolder.author;
        folder.ignore = Boolean(iFolder.ignore);

        if (iFolder.rules) {
            for (const rule of iFolder.rules) {
                folder.rules.push(this.constructRule(rule, parent));
            }
        }

        if (iFolder.children) {
            for (const children of iFolder.children) {
                folder.children.push(this.constructFolder(children, folder));
            }
        }

        if (iFolder.resolvers) {
            for (const resolver of iFolder.resolvers) {
                folder.resolvers.push(this.constructResolver(resolver, parent));
            }
        }

        // Ignore folder if it has no rules and no children
        // No need to analyse the folder to do nothing with it
        if (folder.rules.length === 0 && folder.children.length === 0) {
            folder.ignore = true;
        }

        return folder;
    }

    private findResolver(id: string, parent?: Folder): Resolver {
        let resolver: Resolver | undefined;
        if (parent) {
            resolver = parent.searchResolver(id);
        }

        if (!resolver) {
            resolver = this.config.searchResolver(id);
        }

        if (!resolver) {
            throw new Error('Resolver ' + id + ' not found');
        }

        return resolver;
    }

    private createResolver(iResolver: IResolver): Resolver {
        const resolver = new Resolver();
        resolver.id = iResolver.id;
        resolver.name = iResolver.name;
        resolver.description = iResolver.description;
        resolver.author = iResolver.author;
        resolver.moveTo = iResolver.moveTo;
        resolver.moveItems = iResolver.moveItems;
        resolver.delete = iResolver.delete;
        resolver.deleteItems = iResolver.deleteItems;
        resolver.custom = iResolver.custom;

        return resolver;
    }

    private findRule(iRule: TRule, parent?: Folder): Rule {
        let rule: Rule;

        if (typeof iRule === 'string') {
            // It's a ref
            const referencedRule = parent ? parent.searchRule(iRule) : this.config.searchRule(iRule);
            if (!referencedRule) {
                throw new Error('Rule ' + iRule + ' not found');
            }

            rule = referencedRule.clone();
        } else if ((iRule as IRuleRef).refId) {
            // It's also a ref
            const rRule: IRuleRef = iRule as IRuleRef;
            const referencedRule = parent ? parent.searchRule(rRule.refId) : this.config.searchRule(rRule.refId);

            if (!referencedRule) {
                throw new Error('Rule ' + rRule.refId + ' not found');
            }

            rule = referencedRule.clone();
            rule.name = rRule.name || rule.name;
            rule.description = rRule.description || rule.description;
            rule.author = rRule.author || rule.author;
            rule.deep = rRule.deep || rule.deep;
            rule.ignore = rRule.ignore || rule.ignore;
            rule.inverse = rRule.inverse || rule.inverse;

            if (rRule.resolvers) {
                for (const sResolver of rRule.resolvers) {
                    rule.resolvers.push(this.constructResolver(sResolver, parent));
                }
            }

            if (rRule.rules) {
                for (const sRule of rRule.rules) {
                    rule.rules.push(this.constructRule(sRule, parent));
                }
            }
        } else {
            throw new Error('Can\'t find the original rule because it\'s not a reference rule');
        }

        return rule;
    }

    private createRule(iRule: IRuleDef, parent?: Folder): Rule {
        let rule: Rule;
        const def: IRuleDef = iRule as IRuleDef;

        if (!def.type) {
            throw new Error('Rule must have a type');
        }

        rule = new Rule(def.options);
        rule.type = def.type;
        rule.name = def.name;
        rule.description = def.description;
        rule.id = def.id;
        rule.name = def.name;
        rule.deep = def.deep;
        rule.ignore = def.ignore;
        rule.inverse = def.inverse;

        if (def.rules) {
            for (const sRule of def.rules) {
                rule.rules.push(this.constructRule(sRule, parent));
            }
        }

        if (def.resolvers) {
            for (const sResolver of def.resolvers) {
                rule.resolvers.push(this.constructResolver(sResolver, parent));
            }
        }

        return rule;
    }
}
